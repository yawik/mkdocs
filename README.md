# Yawik Documentation

Edit this documentation in [![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/yawik/mkdocs)

## Install

```
git clone https://gitlab.com/yawik/mkdocs.git
cd mkdocs
mkdocs serve
```

