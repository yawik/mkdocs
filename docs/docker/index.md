# Docker

a [docker Image](https://gitlab.com/yawik/docker/build) allows you
to easily create a developmemnt enviroment that consists of one image for PHP, one for a mongo database, 
and one for Behat tests.

You can:

``` bash
git clone https://gitlab.com/yawik/docker/build.git MyYawikEnv
cd MyYawikEnv/
sudo docker-compose up
```

A [docker-compose](https://gitlab.com/yawik/docker/build/-/blob/latest/docker-compose.yml) file will create an enviroment with a mongo 
server and a selenium instance to run functional tests. You can enter the php container via `docker exec`:

## PHP Container

``` bash
sudo docker exec -it php /bin/bash
su - yawik  # container contains the unprivileged user yawik
```

the php container can access the mongo container using the hostname `mongo`. This can be checked via:

``` bash
curl http://mongo:27017
It looks like you are trying to access MongoDB over HTTP on the native driver port.
```

within the container you can run phpunit or behat tests. 

```
root@php:/# su - yawik
yawik@php:~$ git clone https://gitlab.com/yawik/yawik.git
yawik@php:~$ cd yawik/
yawik@php:~/yawik$ composer install
```

## Mongo Container

``` bash
sudo docker exec -it mongo /bin/bash
```
