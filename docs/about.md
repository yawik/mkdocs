---
title: About
---
# Let's improve Yawik Documentaion

This documentation is intended to improve the [old Yawik documentation](https://yawik.rtfd.org) and at the end replace the sphinx based documentation.

The Goal:

* move documentation to the code.
  Program code and documentation in one repository makes versioning easier.
* Enables contributions via gitpod.
  we want to make it easier to improve documentation.




