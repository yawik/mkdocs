# Requirements

You only need a browser. Because you can edit the documentation in Gitpod.

[Edit in Gitpod](https://gitpod.io/#https://gitlab.com/yawik/mkdocs){: .md-button }

But if you prefer to work locally, then you need [Python](https://www.python.org/) and 
[MkDocs](https://www.mkdocs.org/). Clone this [Repository](https://gitlab.com/yawik/mkdocs/), install your [vscode](https://code.visualstudio.com/) (or whatever to edit) and start contributing.

<pre>
pip install mkdocs
git clone https://gitlab.com/yawik/mkdocs/
cd mkdocs
mkdocs serve
</pre>