# Quickstart

create a directory `docs/` in your yawik module using `mkdocs`.

<pre>
mkdocs new .
mkdocs serve
</pre>

This will create  the mkcocs configuration file `./mkdocs.yml` and an initial index
page `./docs/index.md`
